import { Button } from '../_components/Button'
import { Gutter } from '../_components/Gutter'
import { VerticalPadding } from '../_components/VerticalPadding'

export default function NotFound() {
  return (
    <Gutter>
      <VerticalPadding top="none" bottom="large">
        <h1 style={{ marginBottom: 0 }}>404</h1>
        <p>La page que vous recherchez n'exista pas ou a été supprimée</p>
        <Button href="/" label="Retour à l'accueil" appearance="primary" />
      </VerticalPadding>
    </Gutter>
  )
}
